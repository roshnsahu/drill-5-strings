// ==== String Problem #3 ====
// Given a string in the format of "10/1/2021",
// print the month in which the date is present in.

function month(string){
    let output = string.split('/')
    return output[0]
}

module.exports = month;