// ==== String Problem #1 ====
// There are numbers that are stored in the format "$100.45", "$1,002.22", "-$123", and so on. 
// Write a function to convert the given strings into their equivalent numeric format without any precision loss 
// - 100.45, 1002.22, -123 and so on. 
// There could be typing mistakes in the string so if the number is invalid, return 0. 

let update = [];

function numeric_format(money){


    for (let index = 0; index < money.length; index++){
        
        const output = money[index].split('$')

        if (output.includes('') && output[1].includes(',')){

            update.push(parseFloat(output.join('').trim().replace(',','')))

        } else if(output.includes('') || output.includes('-') ){

            update.push(parseFloat(output.join('')))

        } else{

            update.push(0);
        
        }

    }

    return update

}

module.exports = numeric_format;