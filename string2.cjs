// ==== String Problem #2 ====
// Given an IP address - "111.139.161.143". Split it into its component parts 111, 139, 161, 143 and return it in an array in numeric values. [111, 139, 161, 143].

// Support IPV4 addresses only. If there are other characters detected, return an empty array.

function numericIpAddress(address) {

    let numericIp = address.split('.')

    for (let nums in numericIp) {

        let number = parseInt(numericIp[nums]);

        if (numericIp.length === 4 && 
            (number >= 0 && number <= 255) && 
            String(number).length === numericIp[nums].length) {

            numericIp[nums] = parseInt(numericIp[nums])

        } else {

            return [];

        }
    }

    return numericIp

}

module.exports = numericIpAddress;