// ==== String Problem #4 ====
// Given an object in the following format, return the full name in title case.
// {"first_name": "JoHN", "last_name": "SMith"}
// {"first_name": "JoHN", "middle_name": "doe", "last_name": "SMith"}

function title(obj){

    fullName = '';

    for(let key in obj){
        fullName = fullName + ' ' + obj[key]
    }

    return fullName

}

module.exports = title;